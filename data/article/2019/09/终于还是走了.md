[prop:title]: 终于还是走了
[prop:date]: 2019年9月6日
[prop:tags]: life

## 终于还是走了<br>
终究还是离职了<br>
<img src='/me/imgs/201909/panpan.png' /><br>
再见，熊小猫!<br>
走出办公室，有点心口疼，这是第二次这样的感觉，第一次是在一次失恋之后。时间总能冲淡一切，祝好！